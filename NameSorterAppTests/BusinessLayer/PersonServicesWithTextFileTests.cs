﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NameSorterApp.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameSorterApp.BusinessLayer.Tests
{
    [TestClass()]
    public class PersonServicesWithTextFileTests
    {
        [TestMethod()]
        public void PersonServicesWithTextFileTest()
        {
            PersonServicesWithTextFile service = new PersonServicesWithTextFile("hello");
            Assert.IsNotNull(service);
        }

        [TestMethod()]
        public void GetPersonsTest()
        {
            PersonServicesWithTextFile service = new PersonServicesWithTextFile("hello");
            Assert.IsNotNull(service.GetPersons());
        }

        [TestMethod()]
        public void GetPersonsSortedByLastnameTest()
        {
            PersonServicesWithTextFile service = new PersonServicesWithTextFile("hello");
            Assert.IsNotNull(service.GetPersonsSortedByLastname());
        }
        
    }
}