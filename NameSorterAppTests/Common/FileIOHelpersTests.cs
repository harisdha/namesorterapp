﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NameSorterApp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameSorterApp.Common.Tests
{
    [TestClass()]
    public class FileIOHelpersTests
    {
        [TestMethod()]
        public void GetFileContentByFilepathTest_NullParam()
        {
            FileIOHelpers oHelpers = new FileIOHelpers();
            List<string> list = oHelpers.GetFileContentByFilepath(null);
            Assert.AreEqual(list.Count, 0);
        }

        [TestMethod()]
        public void GetFileContentByFilepathTest_InvalidFilepath()
        {
            FileIOHelpers oHelpers = new FileIOHelpers();
            List<string> list = oHelpers.GetFileContentByFilepath("<><,.?<<{p][LL1239123.txt");
            Assert.AreEqual(list.Count, 0);
        }

        [TestMethod()]
        public void WriteToFileTest()
        {
            FileIOHelpers oHelpers = new FileIOHelpers();
            oHelpers.WriteToFile(null, @"<><>@!#>@!<#>1239123.txt");
            oHelpers.WriteToFile(null, @"test2.txt");

            List<string> list = new List<string>();
            for (int i = 0; i < 1000; i++)
            {
                list.Add(i.ToString());
            }
            oHelpers.WriteToFile(list, @"test.txt");
        }

        [TestMethod()]
        public void IsValidFilenameTest()
        {
            FileIOHelpers oHelpers = new FileIOHelpers();
            Assert.AreEqual(false, oHelpers.IsValidFilename(null));
            Assert.AreEqual(true, oHelpers.IsValidFilename("testc.txt"));
            Assert.AreEqual(false, oHelpers.IsValidFilename("<>AS>D<A>SD<.txt"));
            Assert.AreEqual(false, oHelpers.IsValidFilename("1#@!#@!$@!$!@#$@!%^()"));
            Assert.AreEqual(false, oHelpers.IsValidFilename("?>?<>:\"?!@#>"));
        }

        [TestMethod()]
        public void IsFileExistsTest()
        {
            FileIOHelpers oHelpers = new FileIOHelpers();
            Assert.AreEqual(false, oHelpers.IsFileExists(null));
        }

    }
}