﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NameSorterApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameSorterApp.Model.Tests
{
    [TestClass()]
    public class PersonTests
    {
        [TestMethod()]
        public void PersonTest_CreateInstanceWithNullValue()
        {
            Person oPerson = new Person(null); 
            Assert.AreEqual(oPerson.GetFullname(), null);
            Assert.AreEqual(oPerson.GetLastName(), null);
        }

    }
}