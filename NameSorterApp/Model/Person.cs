﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameSorterApp.Model
{
    public class Person
    {
        private string Fullname;

        /// <summary>
        /// initiate person with fullname
        /// </summary>
        /// <param name="fullname"></param>
        public Person(string fullname)
        {
            this.Fullname = fullname;
        }

        /// <summary>
        /// return the fullname of person
        /// </summary>
        /// <returns></returns>
        public string GetFullname()
        {
            return Fullname;
        }

        /// <summary>
        /// return the lastname of person
        /// </summary>
        /// <returns></returns>
        public string GetLastName()
        {
            if (!string.IsNullOrEmpty(Fullname))
            {
                var spliter = Fullname.Split(' ');
                return spliter[spliter.Length - 1];
            }
            return null;
        }

    }
}
