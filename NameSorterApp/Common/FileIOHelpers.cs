﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NameSorterApp.Common
{
    public class FileIOHelpers
    {
        /// <summary>
        /// return file content in file
        /// </summary>
        /// <param name="filePath">eg.: .\sample.txt</param>
        /// <returns></returns>
        public List<string> GetFileContentByFilepath(string filePath)
        {
            List<string> res = new List<string>();
            if(IsFileExists(filePath))
            {
                FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                using (StreamReader reader = new StreamReader(fileStream))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        res.Add(line);
                    }
                }
            }
            return res;
        }

        /// <summary>
        /// Check file is exists
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public bool IsFileExists(string filePath)
        {
            if (File.Exists(filePath))
                return true;
            else
                return false;   
        }

        /// <summary>
        /// write output file
        /// </summary>
        /// <param name="list"></param>
        /// <param name="OutputFilename"></param>
        public void WriteToFile(List<string> list, string OutputFilename)
        {
            if (IsValidFilename(OutputFilename))
            {
                using (var stream = new FileStream(OutputFilename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
                using (var writer = new StreamWriter(stream))
                {
                    // Write from the start of the file
                    stream.Position = 0;

                    if (list != null)
                    {
                        foreach (var fullname in list)
                        {
                            Console.WriteLine(fullname);
                            writer.WriteLine(fullname);
                        }
                    }

                    writer.Flush();

                    // Truncate
                    stream.SetLength(stream.Position);
                }
            }
        }

        /// <summary>
        /// return true if filename is valid
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool IsValidFilename(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                string filenamePattern = @"^[\w\-. ]+$";
                return Regex.IsMatch(filename, filenamePattern);
            }
            else
                return false;

        }
    }
}
