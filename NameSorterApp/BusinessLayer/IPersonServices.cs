﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NameSorterApp.Model;

namespace NameSorterApp.BusinessLayer
{
    public interface IPersonServices
    {
        List<Person> GetPersons();
        List<Person> GetPersonsSortedByLastname();
    }
}
