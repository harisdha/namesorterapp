﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NameSorterApp.Model;
using NameSorterApp.Common;

namespace NameSorterApp.BusinessLayer
{
    public class PersonServicesWithTextFile : FileIOHelpers, IPersonServices
    {
        private string FilePath;

        /// <summary>
        /// initiate with filepath
        /// </summary>
        /// <param name="filePath">filepath</param>
        public PersonServicesWithTextFile(string filePath)
        {
            this.FilePath = filePath;
        }

        /// <summary>
        /// return list<Person> 
        /// </summary>
        /// <returns></returns>
        public List<Person> GetPersons()
        {
            List<Person> res = new List<Person>();
            if(IsFileExists(FilePath))
            {
                var listContent = GetFileContentByFilepath(FilePath);
                foreach (var fullname in listContent)
                {
                    Person person = new Person(fullname);
                    res.Add(person);
                }
            }
            return res;
        }

        /// <summary>
        /// return list<Person> sorted by lastname
        /// </summary>
        /// <returns></returns>
        public List<Person> GetPersonsSortedByLastname()
        {
            return GetPersons().OrderBy(o => o.GetLastName()).ToList();
        }

    }
}
