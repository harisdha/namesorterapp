﻿using NameSorterApp.BusinessLayer;
using NameSorterApp.Common;
using NameSorterApp.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameSorterApp
{
    class Program
    {
        private static string OutputFilename = @"sorted-names-list.txt";

        /// <summary>
        /// add this line for debuging
        /// args = new string[] { @"./unsorted-names-list.txt" };
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //validation args
            if (!IsValidArgs(args))
                return;

            //set new personservices with filepath = args[0] 
            IPersonServices personServices = new PersonServicesWithTextFile(args[0]);

            //get persons sorted by lastname
            List<Person> listPersonSorted = personServices.GetPersonsSortedByLastname();

            //write result into file output
            new FileIOHelpers().WriteToFile(listPersonSorted.Select(s => s.GetFullname()).ToList(), OutputFilename);
        }

        /// <summary>
        /// all validation args goes here
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static bool IsValidArgs(string[] args)
        {
            if (args.Count() == 0)
            {
                Console.WriteLine("Please input filepath in args");
                return false;
            }

            if (!new FileIOHelpers().IsFileExists(args[0]))
            {
                Console.WriteLine("File not exists");
                return false;
            }

            return true;
        }
    }
}
